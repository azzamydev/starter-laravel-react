import { ButtonHTMLAttributes } from "react";
import { Button } from "./ui/button";

export default function SecondaryButton({
    type = "button",
    className = "",
    disabled,
    children,
    ...props
}: ButtonHTMLAttributes<HTMLButtonElement>) {
    return (
        <Button
            variant={"secondary"}
            {...props}
            type={type}
            className={`${disabled && "opacity-25"} ` + className}
            disabled={disabled}
        >
            {children}
        </Button>
    );
}
