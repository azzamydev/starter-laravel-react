import { Badge } from "@/Components/ui/badge";
import { Button } from "@/Components/ui/button";
import { Link, usePage } from "@inertiajs/react";
import {
    Bell,
    Home,
    LineChart,
    Package,
    Package2,
    ShoppingCart,
    Users,
} from "lucide-react";
import { Popover, PopoverContent, PopoverTrigger } from "./ui/popover";
import { ReactNode } from "react";

const Sidebar = () => {
    const { component } = usePage();
    return (
        <div className="flex h-full max-h-screen flex-col gap-2">
            <div className="flex h-14 items-center border-b px-4 lg:h-[60px] lg:px-6">
                <Link
                    href={route("dashboard")}
                    className="flex items-center gap-2 font-semibold"
                >
                    <Package2 className="h-6 w-6" />
                    <span className="">Acme Inc</span>
                </Link>
                <Popover>
                    <PopoverTrigger className="ml-auto">
                        <Button
                            variant="outline"
                            size="icon"
                            className=" h-8 w-8"
                        >
                            <Bell className="h-4 w-4" />
                            <span className="sr-only">
                                Toggle notifications
                            </span>
                        </Button>
                    </PopoverTrigger>
                    <PopoverContent align="start" className="min-w-[500px] p-0">
                        <div className="px-4 py-2 space-y-3 border-b">
                            <h1>Notification</h1>
                        </div>
                        <div className="min-h-[300px]"></div>
                    </PopoverContent>
                </Popover>
            </div>
            <div className="flex-1">
                <nav className="grid items-start px-2 text-sm font-medium lg:px-4">
                    <SidebarItem
                        href={route("dashboard")}
                        label="Dashboard"
                        active={component.startsWith("Dashboard")}
                        icon={<Home className="h-4 w-4" />}
                    />
                    <SidebarItem
                        href="#"
                        label="Order"
                        active={component.startsWith("Order")}
                        icon={<ShoppingCart className="h-4 w-4" />}
                        badge={
                            <Badge className="ml-auto flex h-6 w-6 shrink-0 items-center justify-center rounded-full">
                                6
                            </Badge>
                        }
                    />
                    <SidebarItem
                        href="#"
                        label="Products"
                        active={component.startsWith("Products")}
                        icon={<Package className="h-4 w-4" />}
                    />
                    <SidebarItem
                        href="#"
                        label="Customers"
                        active={component.startsWith("Customers")}
                        icon={<Users className="h-4 w-4" />}
                    />
                    <SidebarItem
                        href="#"
                        label="Analytics"
                        active={component.startsWith("Analytics")}
                        icon={<LineChart className="h-4 w-4" />}
                    />
                </nav>
            </div>
            <div className="mt-auto p-4"></div>
        </div>
    );
};

const SidebarItem = ({
    label,
    href,
    icon,
    active,
    badge,
}: {
    label: string;
    href: string;
    icon: ReactNode;
    active?: boolean;
    badge?: ReactNode;
}) => {
    return (
        <>
            <Link
                href={href}
                className={`flex items-center gap-3 rounded-lg px-3 py-2 ${
                    active ? "bg-muted text-primary" : "text-muted-foreground"
                } transition-all hover:text-primary`}
            >
                {icon}
                {label}
                {badge}
            </Link>
        </>
    );
};

export default Sidebar;
