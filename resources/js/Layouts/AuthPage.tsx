import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { PageProps } from "@/types";
import { ReactNode } from "react";

interface AuthPageProps extends PageProps {
    children: ReactNode;
    title: string | ReactNode;
}
const AuthPage = ({ auth, children, title }: AuthPageProps) => {
    return (
        <AuthenticatedLayout user={auth.user}>
            <main className="flex flex-1 flex-col gap-4 p-4 lg:gap-6 lg:p-6">
                <div className="flex items-center">
                    {typeof title == "string" ? (
                        <h1 className="text-lg font-semibold md:text-2xl">
                            {title}
                        </h1>
                    ) : (
                        title
                    )}
                </div>
                {children}
            </main>
        </AuthenticatedLayout>
    );
};

export default AuthPage;
