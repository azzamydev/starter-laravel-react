import Navbar from "@/Components/Navbar";
import Sidebar from "@/Components/Sidebar";
import { ScrollArea } from "@/Components/ui/scroll-area";
import { User } from "@/types";
import { PropsWithChildren } from "react";

export default function Authenticated({
    user,
    children,
}: PropsWithChildren<{ user: User }>) {
    console.log(user);
    return (
        <div className="grid h-screen w-full md:grid-cols-[220px_1fr] lg:grid-cols-[280px_1fr] overflow-hidden">
            <div className="hidden border-r bg-muted/40 md:block">
                <Sidebar />
            </div>
            <ScrollArea className="flex flex-col bg-slate-100">
                <Navbar />
                {children}
            </ScrollArea>
        </div>
    );
}
